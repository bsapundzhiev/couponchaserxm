﻿using CouponChaserXM.Services;
using CouponChaserXM.ViewModels;
using CouponChaserXM.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace CouponChaserXM
{
    public partial class App : Application
    {
        public static LocatorService locatorService { get; } = new LocatorService();
        /// <summary>
        /// Init dependencies
        /// </summary>
        void AppConfigure()
        {
            NavigationService navigationService = new NavigationService();
            navigationService.Configure(typeof(LoginPage));
            navigationService.Configure(typeof(TabsPage));
            navigationService.Configure(typeof(CouponPage));

            //Dependecies
            locatorService.RegisterService<IDialogService, DialogService>();
            locatorService.RegisterService<INavigationService>(navigationService);
            //ViewModels
            locatorService.RegisterService<MainViewModel>();
            locatorService.RegisterService<LoginViewModel>();
        }

        public App()
        {

            AppConfigure();
            ///IOC test 
            MainViewModel mainViewModel = locatorService.GetService<MainViewModel>();
            mainViewModel.Title = "Test";
            MainViewModel mainViewModel2 = locatorService.GetService<MainViewModel>();

            INavigationService service = locatorService.GetService<INavigationService>();

            LoginViewModel loginViewModel = locatorService.GetService<LoginViewModel>();
            loginViewModel.Title = "Test1";
            LoginViewModel loginViewModel2 = locatorService.GetService<LoginViewModel>();

            InitializeComponent();

            //MainPage = new MapPage();

            MainPage = new NavigationPage(new MainPage());
            //service.NavigateTo("login");
            service.NavigateTo(typeof(TabsPage));
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
