﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CouponChaserXM.Views
{
    public class ViewPage<T> : ContentPage 
    {
        readonly T _viewModel;
        public T ViewModel
        {
            get { return _viewModel; }
        }

        public ViewPage()
        {
            _viewModel = App.locatorService.GetService<T>(); 
            BindingContext = _viewModel;
        }
    }
}
