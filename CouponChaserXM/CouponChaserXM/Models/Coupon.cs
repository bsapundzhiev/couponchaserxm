﻿using CouponChaserXM.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouponChaserXM.Models
{
    public class Coupon : ObservableObject
    {
        public Coupon() 
        {
        }

        public string Text
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

       
        public string Description
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }
    }
}
