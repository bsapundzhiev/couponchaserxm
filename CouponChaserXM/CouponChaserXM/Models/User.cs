﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouponChaserXM.Models
{

    public class User
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }

        public IEnumerable<string> Information { get; set; }

        public User()
        {
        }

        public User(string name, string surname, IEnumerable<string> information)
        {
            Name = name;
            Surname = surname;
            Information = information;
        }
    }
}
