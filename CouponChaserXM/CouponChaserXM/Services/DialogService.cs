﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CouponChaserXM.Services
{
    /// <summary>
    /// Dialog service
    /// </summary>
    /// Idea from https://forums.xamarin.com/discussion/48009/pop-display-alert-message-from-viewmodel
    ///
    public class DialogService : IDialogService
    {
        public DialogService()
        {
            Debug.WriteLine("DialogService()");
        }

        public async Task<bool> ShowMessage(
            string message,
            string title,
            string buttonConfirmText,
            string buttonCancelText,
            Action<bool> afterHideCallback)
        {
            var result = false;
            if(buttonCancelText != null)
                result = await Application.Current.MainPage.DisplayAlert(title, message,buttonConfirmText,buttonCancelText);
            else 
                await Application.Current.MainPage.DisplayAlert(title, message, buttonConfirmText);

            afterHideCallback?.Invoke(result);

            return result;
        }

        public void ShowAlert(string message)
        {
            Device.BeginInvokeOnMainThread(() =>{
                Application.Current.MainPage.DisplayAlert("Alert", message, "OK");
            });
        }
    }
}
