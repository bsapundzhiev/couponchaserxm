﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouponChaserXM.Services
{
    /// <summary>
    /// Dialog service
    /// </summary>
    interface IDialogService
    {
        void ShowAlert(string message);

        Task<bool> ShowMessage(
            string message,
            string title,
            string buttonConfirmText,
            string buttonCancelText,
            Action<bool> afterHideCallback);
    }
}
