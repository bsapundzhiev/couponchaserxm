﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouponChaserXM.Services
{
    public interface INavigationService
    {
        void GoBack();
        void NavigateTo(Type page);
        void NavigateTo(Type page, object parameter);
        void NavigateTo(Type page, object parameter, HistoryBehavior historyBehavior);
    }
}
