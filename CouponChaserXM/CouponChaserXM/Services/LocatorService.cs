﻿using CouponChaserXM.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouponChaserXM.Services
{
    //Xamarin.Forms.DependencyService.Register

    public class LocatorService : IServiceProvider
    {
        IUnityContainer container;

        public LocatorService()
        {
            container = new UnityContainer();
        }

        //Register dependency
        public void RegisterService<T, TImpl>() where T : class where TImpl : class, T
        {
            container.RegisterType<T, TImpl>(new ContainerControlledLifetimeManager());
        }

        public void RegisterService<T>() where T: class
        {
            container.RegisterType<T>(new ContainerControlledLifetimeManager());
        }

        public void RegisterService(object service)
        {
            container.RegisterInstance(service);
        }

        public void RegisterService<T>(T serviceInstance)
        {
            container.RegisterInstance<T>(serviceInstance);
        }

        public object GetService(Type serviceType)
        {
            return container.Resolve(serviceType);
        }

        public T GetService<T>()
        {
            return container.Resolve<T>();
        }

    }

}
