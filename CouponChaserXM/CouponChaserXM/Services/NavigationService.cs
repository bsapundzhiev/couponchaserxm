﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CouponChaserXM.Services
{
    public enum HistoryBehavior
    {
        Default,
        ClearHistory
    }

    class NavigationService: INavigationService
    {
        private Dictionary<string, Type> pages { get; }
            = new Dictionary<string, Type>();

        public Page MainPage => Application.Current.MainPage;
        
        private static ConditionalWeakTable<Page, object> arguments
            = new ConditionalWeakTable<Page, object>();
        object GetNavigationArgs(Page page)
        {
            object argument = null;
            arguments.TryGetValue(page, out argument);

            return argument;
        }

        void SetNavigationArgs(Page page, object args)
            => arguments.Add(page, args);
       
        public void Configure(Type pageType) => pages[pageType.Name] = pageType;

        public void GoBack()
        {
            MainPage.Navigation.PopAsync();
        }

        public void NavigateTo(Type page)
        {
            NavigateTo(page, null, HistoryBehavior.Default);
        }

        public void NavigateTo(Type page, object parameter)
        {
            NavigateTo(page, parameter, HistoryBehavior.Default);
        }

        public void NavigateTo(Type page, object parameter, HistoryBehavior historyBehavior)
        {
            Type pageType;
            if (pages.TryGetValue(page.Name, out pageType))
            {
                Page displayPage = (Page)Activator.CreateInstance(pageType);
                if(parameter != null)
                {
                    SetNavigationArgs(displayPage, parameter);
                }
                
                if (historyBehavior == HistoryBehavior.ClearHistory)
                {
                    MainPage.Navigation.InsertPageBefore(displayPage,
                        MainPage.Navigation.NavigationStack[0]);

                    var existingPages = MainPage.Navigation.NavigationStack.ToList();
                    for (int i = 1; i < existingPages.Count; i++)
                        MainPage.Navigation.RemovePage(existingPages[i]);
                }
                else
                {
                    MainPage.Navigation.PushAsync(displayPage);
                }
            }
            else
            {
                throw new ArgumentException($"No such page: {page.Name}.", nameof(page));
            }
        }
    }
}
