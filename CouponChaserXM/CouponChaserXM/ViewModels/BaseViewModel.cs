﻿using CouponChaserXM.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CouponChaserXM.ViewModels
{
    public class BaseViewModel : ObservableObject
    {
        public BaseViewModel() : base()
        {

        }

        public string Title
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }
    }
}

