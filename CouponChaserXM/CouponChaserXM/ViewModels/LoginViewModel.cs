﻿using CarExpenses.Helpers;
using CouponChaserXM.Models;
using CouponChaserXM.Services;
using CouponChaserXM.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CouponChaserXM.ViewModels
{
    class LoginViewModel : BaseViewModel
    {
        //Dependecies
        private INavigationService _navigationService;
        private IDialogService _dialogService;
        //Bindings
        public ICommand LoginCommand { protected set; get; }

        public User user
        {
            get { return GetValue<User>(); }
            set { SetValue(value); }
        } 

        public LoginViewModel(INavigationService navigationSerice, IDialogService dialogService)
        {
            _navigationService = navigationSerice;
            _dialogService = dialogService;

            user = new User();
            LoginCommand = new Command<User>(onLogin);
        }

        private void onLogin(User u)
        {
            if(String.IsNullOrEmpty(u.Name) && String.IsNullOrEmpty(u.Password))
            {
                _dialogService.ShowAlert("Test alert");
            }
            else
            {
                _navigationService.NavigateTo(typeof(TabsPage));
            }
           
        }
    }
}
