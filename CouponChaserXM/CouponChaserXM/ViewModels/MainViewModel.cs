﻿using CouponChaserXM.Models;
using CouponChaserXM.Services;
using CouponChaserXM.Views;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace CouponChaserXM.ViewModels
{
    class MainViewModel: BaseViewModel
    {
        private INavigationService _navigationService;
        private IDialogService _dialogService;
        
        //Bindings
        public ICommand SelectCouponCommand { protected set; get; }
        public ObservableCollection<Coupon> CouponItems { get; set; }

        public Coupon SelectedCoupon
        {
            get
            {
                return GetValue<Coupon>();
            }
            set
            {
                SetValue(value);
                OnCouponSelected(value);
            }
        }

        public MainViewModel(INavigationService navigationSerice, IDialogService dialogService)
        {
            _navigationService = navigationSerice;
            _dialogService = dialogService;
            
            CouponItems = new ObservableCollection<Coupon>() {
                { new Coupon { Text= "Coupon1", Description ="Description1"} },
                { new Coupon { Text= "Coupon2", Description= "Description2"} },
                { new Coupon { Text= "Coupon3", Description= "Description3"} },
                { new Coupon { Text= "Coupon4", Description= "Description4"} },
            };
        }

        private void  OnCouponSelected(Coupon c)
        {
            if (SelectedCoupon != null)
            {
                _navigationService.NavigateTo(typeof(CouponPage));
                //SelectedCoupon = null;
            }
        }
    }
}
