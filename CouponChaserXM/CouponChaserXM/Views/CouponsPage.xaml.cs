﻿using CouponChaserXM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CouponChaserXM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CouponsPage : ContentPage
    {
        public CouponsPage()
        {
            InitializeComponent();
            BindingContext = App.locatorService.GetService<MainViewModel>();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //((MainViewModel)this.BindingContext).SelectedCoupon = null;
            //listView.SelectedItem = null;
        }
    }
}
