﻿
using CouponChaserXM.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CouponChaserXM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilterPage : ContentPage
    {
        public FilterPage()
        {
            InitializeComponent();
            this.BindingContext = App.locatorService.GetService<MainViewModel>();
        }
    }
}
